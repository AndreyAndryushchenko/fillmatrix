package org.fillmatrix

import java.lang.IllegalArgumentException
import kotlin.properties.Delegates

/**
 * Builder for matrix
 */
class FinalMatrix {
    private var dim by Delegates.notNull<Int>()
    private var sum by Delegates.notNull<Int>()
    private var lastSum = 0
    private var counter = 1
    private var branchNumber = 0
    private var elementBranchNumber = 1
    private var row = 0
    private var column = 0

    fun buildSize(dim: Int) {
        this.dim = dim
        sum = 4 * (dim - 1 - 2 * branchNumber)
    }

    fun buildCell(m: Matrix) {
        if (counter > sum) {
            branchNumber++
            lastSum = sum
            sum += 4 * (dim - 1 - 2 * branchNumber)
        }

        elementBranchNumber = counter - lastSum

        if (elementBranchNumber <= dim - 2 * branchNumber) {
            row = branchNumber
            column = branchNumber + elementBranchNumber - 1
        } else if (elementBranchNumber > dim - 2 * branchNumber && elementBranchNumber <= (sum - lastSum) / 2 + 1) {
            row = elementBranchNumber - (dim - 2 * branchNumber) + branchNumber
            column = dim - branchNumber - 1
        } else if (elementBranchNumber <= sum - lastSum - (dim - 2 * (branchNumber + 1)) && elementBranchNumber > (sum - lastSum) / 2 + 1) {
            row = dim - 1 - branchNumber
            column = dim - (elementBranchNumber - ((sum - lastSum) / 2 + 1)) - 1 - branchNumber
        } else if (elementBranchNumber <= sum - lastSum && elementBranchNumber > sum - lastSum - (dim - 2 * (branchNumber + 1))) {
            row = sum - lastSum - elementBranchNumber + 1 + branchNumber
            column = branchNumber
        }
        if (branchNumber % 2 == 0) {
            m.set(column, row, counter)
        } else {
            m.set(row, column, counter)
        }
        counter++
    }
}

/**
 * Matrix filling according rule
 * This function filling matrix by rule, for example to matrix 7x7:
 *  1 24 23 22 21 20 19
 *  2 25 26 27 28 29 18
 *  3 40 41 48 47 30 17
 *  4 39 42 49 46 31 16
 *  5 38 43 44 45 32 15
 *  6 37 36 35 34 33 14
 *  7  8  9 10 11 12 13
 * @param matrix matrix sizeof dim^2
 * @param dim matrix dimension
 * @return matrix filled according rule
 */
fun fillMatrix(matrix: Matrix, dim: Int) {
    if (dim <= 0) {
        throw IllegalArgumentException("Dim shouldn't be lower or equal 0")
    }
    val builder = FinalMatrix()
    builder.buildSize(dim)
    IntRange(0, dim * dim - 1).forEach {
        builder.buildCell(matrix)
    }
}
