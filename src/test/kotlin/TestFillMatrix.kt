package org.fillmatrix

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestFillMatrix {

    @Test
    fun `dim equals 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `dim lower than 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 3 x 3`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 3)
        //      0   1   2
        //     __  __  __
        // 0  | 1,  8,  7,
        // 1  | 2,  9,  6,
        // 2  | 3,  4,  5
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(1, 0, 2)
        Mockito.verify(m).set(2, 0, 3)
        Mockito.verify(m).set(2, 1, 4)
        Mockito.verify(m).set(2, 2, 5)
        Mockito.verify(m).set(1, 2, 6)
        Mockito.verify(m).set(0, 2, 7)
        Mockito.verify(m).set(0, 1, 8)
        Mockito.verify(m).set(1, 1, 9)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 4)
        //      0   1   2   3
        //     __  __  __  __
        // 0  | 1, 12, 11, 10,
        // 1  | 2, 13, 14,  9,
        // 2  | 3, 16, 15,  8,
        // 3  | 4,  5,  6,  7
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(1, 0, 2)
        Mockito.verify(m).set(2, 0, 3)
        Mockito.verify(m).set(3, 0, 4)

        Mockito.verify(m).set(3, 1, 5)
        Mockito.verify(m).set(3, 2, 6)
        Mockito.verify(m).set(3, 3, 7)

        Mockito.verify(m).set(2, 3, 8)
        Mockito.verify(m).set(1, 3, 9)
        Mockito.verify(m).set(0, 3, 10)

        Mockito.verify(m).set(0, 2, 11)
        Mockito.verify(m).set(0, 1, 12)

        Mockito.verify(m).set(1, 1, 13)
        Mockito.verify(m).set(1, 2, 14)

        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(2, 1, 16)
    }
}
